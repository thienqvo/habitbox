import 'styled-components';
import { Dimensions } from 'react-native';
import { range } from 'lodash';
import Color from 'color';
import * as _ from 'styled-components/cssprop';

const normalizeFontSize = (md: number, lg: number) => {
  if (Dimensions.get('window').width > 375) {
    return lg;
  }
  return md;
};
const createColorPallete = (inputColors: Record<string, string>) => {
  const colors = { ...inputColors };
  const percentageRange = range(0, 101);
  Object.keys(colors).forEach((name) => {
    percentageRange.forEach((p) => {
      const newName = `${name}-${p}`;
      const newColor = Color(colors[name])
        .alpha(Math.max(0, p / 100))
        .toString();
      colors[newName] = newColor;
    });
  });
  return colors;
};

const pallete = {
  apple: '#EE2233',
  bittersweet: '#FF595E',
  gogreen: '#0EAD69',
  summer: '#BBFFEE',
  skydive: '#1D74F1',
  azure: '#74BBFB',
  orangepantone: '#FB5607',
  wintersky: '#F53566',
  blueviolet: '#542FBC',
  mango: '#F8BC32',
  black: '#000000',
  night: '#232323',
  beluga: '#353535',
  possum: '#4F4F4F',
  shilling: '#A3A3A3',
  cumulus: '#F3F3F3',
  white: '#FFFFFF',
};

export const defaultTheme = {
  colors: createColorPallete(pallete) as typeof pallete &
    Record<string, string>,
  // px in design
  fontSizes: {
    get xs() {
      return normalizeFontSize(14, 16);
    },
    get sm() {
      return normalizeFontSize(16, 18);
    },
    get md() {
      return normalizeFontSize(18, 20);
    },
    get lg() {
      return normalizeFontSize(21, 23);
    },
    get xl() {
      return normalizeFontSize(23, 25);
    },
    get xxl() {
      return normalizeFontSize(27, 29);
    },
    get '3xl'() {
      return normalizeFontSize(31, 33);
    },
    get '4xl'() {
      return normalizeFontSize(35, 37);
    },
    get timerSize() {
      return normalizeFontSize(52, 54);
    },
  },
  fonts: {
    displayRegular: 'SFProDisplay-Regular',
    displayBold: 'SFProDisplay-Bold',
    regular: 'SFProText-Regular',
    medium: 'SFProText-Medium',
    semibold: 'SFProText-Semibold',
    bold: 'SFProText-Bold',
  },
  space: [
    '0px', // 0
    '4px', // 1
    '8px', // 2
    '12px', // 3
    '16px', // 4
    '20px', // 5
    '24px', // 6
    '32px', // 7
    '40px', // 8
    '48px', // 9
    '64px', // 10
  ],
};

export const getColorFromTheme = (maybeInPalleteName: string) => {
  if (maybeInPalleteName in defaultTheme.colors) {
    return defaultTheme.colors[maybeInPalleteName];
  }
  return maybeInPalleteName;
};

export const getTheme = (_kind: ThemeKind = 'default') => {
  return defaultTheme;
};

export const getHeightByFontSize = (size: keyof ThemeStyle['fontSizes']) => {
  return 8 + 1.5 * defaultTheme.fontSizes[size];
};

// -TYPES-
type ThemeKind = 'default' | 'dark';
export type ThemeStyle = typeof defaultTheme;

declare module 'styled-components' {
  export interface DefaultTheme extends ThemeStyle {}
}
