import { useEffect, useRef } from 'react';

// Hook
export function usePrevious<T>(value: T) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef<T>();

  // Store current value in ref
  useEffect(() => {
    ref.current = value;
  }, [value]); // Only re-run if value changes

  // Return previous value (happens before update in useEffect above)
  return ref.current;
}
type NonUndefined<T> = T extends undefined ? never : T;

export function usePreviousConditional<T extends NonUndefined<any>>(
  nextValue: T,
  shouldSetNewValue: (currentValue: T, nextValue: T) => boolean
) {
  const ref = useRef<T>();

  const currentValue = ref.current;
  const shouldUpdateRef =
    currentValue !== undefined && shouldSetNewValue(currentValue, nextValue);

  useEffect(() => {
    if (shouldUpdateRef) {
      ref.current = nextValue;
    }
  }, [nextValue, shouldUpdateRef]);

  return currentValue;
}
