import 'react-native-gesture-handler';
import 'mobx-react/batchingForReactNative';
import 'react-native-get-random-values';

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider } from 'styled-components/native';

import { getTheme } from './theme';
import { StoreProvider } from './stores/StoreProvider';
import { observer } from 'mobx-react';
import { AppScreens } from './views/RoutineScreen';

const App = observer(() => {
  return (
    <NavigationContainer>
      <StoreProvider>
        <ThemeProvider theme={getTheme('default')}>
          <AppScreens />
        </ThemeProvider>
      </StoreProvider>
    </NavigationContainer>
  );
});

export default App;
declare const global: { HermesInternal: null | {} };
