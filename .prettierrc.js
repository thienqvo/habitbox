module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: false,
  singleQuote: true,
  trailingComma: 'es5',
  printWidth: 80,
  useTabs: false,
  tabWidth: 2,
  semi: true,
  quoteProps: 'as-needed',
  jsxSingleQuote: false,
  arrowParens: 'always',
};
