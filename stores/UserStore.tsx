import { inRange, round, sum } from 'lodash';
import { v4 as uuid } from 'uuid';
import {
  types,
  Instance,
  SnapshotIn,
  flow,
  cast,
  getSnapshot,
  onSnapshot,
} from 'mobx-state-tree';
import Color from 'color';
import AsyncStorage from '@react-native-community/async-storage';

import { getDurationText } from '../components/utils';
import { IllustrationNames } from '../components/IllustrationVectors';

const uniqueId = (prefix: string = '') => `${prefix}${uuid()}`;

const SessionHabitMetadata = types.model({
  startedTime: types.maybeNull(types.number),
  completedTime: types.maybeNull(types.number),
});
type SessionHabitStatus = 'QUEUED' | 'ACTIVE' | 'COMPLETED' | 'SKIPPED';
const SessionHabit = types
  .model({
    id: types.optional(types.identifier, () => uniqueId('SessionHabit-')),
    status: types.enumeration(['QUEUED', 'ACTIVE', 'COMPLETED', 'SKIPPED']),
    displayName: types.string,
    displayImageKey: types.string,
    accentColor: types.string,
    originalDuration: types.number,
    addedDuration: types.number,
    elapsedTime: types.number,
    metadata: types.optional(SessionHabitMetadata, {}),
  })
  .views((self) => ({
    get totalDuration() {
      return self.originalDuration + self.addedDuration;
    },
    get remainDuration() {
      return this.totalDuration - self.elapsedTime;
    },
    get progressPercentage() {
      return round(
        (this.totalDuration - this.remainDuration) / this.totalDuration,
        2
      );
    },
    hasStatusOf(...args: Array<SessionHabitStatus>) {
      return args.includes(self.status);
    },
  }))
  .actions((self) => ({
    tick() {
      self.elapsedTime += 1;
    },
    setStatus(status: SessionHabitStatus) {
      self.status = status;
    },
  }));

const SessionMetadata = types.model({
  startedTimestamp: types.maybeNull(types.number),
  endTimestamp: types.maybeNull(types.number),
});
const Session = types
  .model({
    id: types.optional(types.identifier, () => uniqueId('Session-')),
    status: types.enumeration(['ACTIVE', 'PAUSED']),
    displayName: types.string,
    // TODO: make this a reference
    habitQueue: types.array(SessionHabit),
    currentHabit: types.maybeNull(types.reference(SessionHabit)),
    metadata: types.optional(SessionMetadata, {}),
  })
  .views((self) => ({
    get isPaused() {
      return self.status === 'PAUSED';
    },
    get currentHabitIndex() {
      return self.habitQueue.findIndex(
        (sessionHabit) => sessionHabit.id === self.currentHabit?.id
      );
    },
    get upNextHabits() {
      const nextHabitIndex = this.currentHabitIndex + 1;
      if (inRange(nextHabitIndex, 0, self.habitQueue.length)) {
        return self.habitQueue.slice(nextHabitIndex);
      }
      return [];
    },
    get completedOrSkippedHabits() {
      return self.habitQueue.filter((h) =>
        h.hasStatusOf('COMPLETED', 'SKIPPED')
      );
    },
  }))
  .views((self) => ({
    get allTotalDuration() {
      return sum(self.habitQueue.map((h) => h.totalDuration));
    },
    get allTotalElapsedTime() {
      return sum(self.habitQueue.map((h) => h.elapsedTime));
    },
  }))
  .actions((self) => ({
    begin() {
      self.status = 'ACTIVE';
      self.metadata.startedTimestamp = Date.now();
      if (self.currentHabit) {
        self.currentHabit.setStatus('ACTIVE');
      } else if (self.upNextHabits.length) {
        self.currentHabit = self.upNextHabits[0];
        self.currentHabit.setStatus('ACTIVE');
      }
    },
    end() {
      self.status = 'PAUSED';
      self.metadata.endTimestamp = Date.now();
      if (self.currentHabit) {
        self.currentHabit = null;
      }
    },
    pause() {
      self.status = 'PAUSED';
    },
    unpause() {
      self.status = 'ACTIVE';
    },
    addMoreTime(addedDuration: number) {
      if (self.currentHabit) {
        self.currentHabit.addedDuration += addedDuration;
      }
    },
    goToNextHabit(method: 'SKIPPED' | 'COMPLETED') {
      if (self.currentHabit) {
        self.currentHabit.status = method;
      }
      if (self.upNextHabits[0]) {
        self.currentHabit = self.upNextHabits[0];
        self.currentHabit.setStatus('ACTIVE');
      }
    },
  }));

export const Habit = types
  .model({
    id: types.optional(types.identifier, () => uniqueId('Habit-')),
    name: types.string,
    duration: types.number,
    color: types.optional(types.string, '#34C759'),
    imageKey: types.optional(types.string, 'code'),
  })
  .views((self) => ({
    get durationText() {
      return getDurationText(self.duration);
    },
  }))
  .actions((self) => ({
    setName(name: string) {
      self.name = name;
    },
    setDuration(duration: number) {
      self.duration = duration;
    },
    setColor(color: string) {
      self.color = color;
    },
    setImageKey(key: string) {
      self.imageKey = key;
    },
    toSessionHabit(status: ISessionHabit['status']): ISessionHabit {
      return SessionHabit.create({
        status,
        displayName: self.name,
        originalDuration: self.duration,
        addedDuration: 0,
        elapsedTime: 0,
        displayImageKey: self.imageKey,
        accentColor: self.color,
      });
    },
  }))
  .actions((self) => ({
    async save() {
      const saveHabitString = JSON.stringify(getSnapshot(self));
      try {
        await AsyncStorage.setItem(self.id, saveHabitString);
        // console.log(`[${self.id}]: Saved`);
      } catch (error) {
        // console.log(`[${self.id}]: Failed to save`);
        // console.error(error);
      }
    },
    afterCreate() {
      this.save();
      onSnapshot(self, this.save);
    },
    beforeDestroy() {
      AsyncStorage.removeItem(self.id);
    },
  }));

export const Routine = types
  .model({
    id: types.optional(types.identifier, () => uniqueId('Routine-')),
    title: types.string,
    displayInfo: types
      .model({
        color: types.string,
        illustrationName: types.optional(
          types.enumeration(IllustrationNames),
          'Flower01'
        ),
      })
      .views((self) => ({
        get primaryColor() {
          return self.color;
        },
        get secondaryColor() {
          return Color(this.primaryColor)
            .saturate(-0.5)
            .lighten(0.5)
            .toString();
        },
      })),
    habits: types.optional(types.array(types.reference(Habit)), []),
  })
  .views((self) => ({
    get totalDurationString() {
      const totalDuration = sum(self.habits.map((h) => h.duration));
      return totalDuration ? getDurationText(totalDuration) : '--';
    },
  }))
  .actions((self) => ({
    async save() {
      const saveRoutineString = JSON.stringify(getSnapshot(self));
      try {
        await AsyncStorage.setItem(self.id, saveRoutineString);
        // console.log(`[${self.id}]: Saved`);
      } catch (error) {
        // console.log(`[${self.id}]: Failed to save`);
        // console.error(error);
      }
    },
    afterCreate() {
      this.save();
      onSnapshot(self, this.save);
    },
    beforeDestroy() {
      AsyncStorage.removeItem(self.id);
    },
  }))
  .actions((self) => ({
    linkHabit(id: string) {
      if (!self.habits.some((h) => h.id === id)) {
        return self.habits.push(id);
      }
    },
    unlinkHabit(id: string) {
      self.habits.replace(self.habits.filter((h) => h.id !== id));
    },
    updateHabits(habits: IHabit[]) {
      self.habits.replace(habits);
    },
    updateWith(
      info: Partial<{
        title: string;
        color: string;
        illustrationName: IllustrationNames;
      }>
    ) {
      const { title, color, illustrationName } = info;
      if (title) {
        self.title = title;
      }
      if (color) {
        self.displayInfo.color = color;
      }
      if (illustrationName) {
        self.displayInfo.illustrationName = illustrationName;
      }
    },
    toSession(): ISession {
      const habitQueue = self.habits.map((h) => h.toSessionHabit('QUEUED'));
      if (habitQueue[0]) {
        habitQueue[0].setStatus('ACTIVE');
      }
      return Session.create({
        status: 'ACTIVE',
        displayName: self.title,
        habitQueue,
        currentHabit: habitQueue[0]?.id,
      });
    },
  }));

export const User = types
  .model({
    id: types.identifier,
    applicationState: types.model({
      status: types.optional(
        types.enumeration([
          'UNINTIALIZED',
          'LOADING',
          'READY',
          'CHANGES_UNSAVED',
          'SAVING',
        ]),
        'UNINTIALIZED'
      ),
      error: types.optional(types.frozen<any>(), null),
    }),
    firstName: types.maybeNull(types.string),
    lastName: types.maybeNull(types.string),
    routines: types.optional(types.array(types.reference(Routine)), []),
  })
  .actions((self) => ({
    save: flow(function* saveSelf() {
      const currentUser = self;
      const saveUserString = JSON.stringify(getSnapshot(currentUser));
      currentUser.applicationState = {
        status: 'SAVING',
        error: null,
      };
      try {
        yield AsyncStorage.setItem(`User-${currentUser.id}`, saveUserString);
        // console.log('Successfully save', currentUser.id);
      } catch (error) {
        // console.log('Failed to save', currentUser.id);
        currentUser.applicationState = {
          status: 'READY',
          error,
        };
      }
    }),
  }))
  .actions((self) => ({
    linkRoutine(id: string) {
      if (!self.routines.some((r) => r.id === id)) {
        return self.routines.push(id);
      }
    },
    unlinkRoutine(id: string) {
      self.routines.replace(self.routines.filter((r) => r.id !== id));
    },
  }));
export const RoutineStore = types
  .model({
    routines: types.optional(types.map(Routine), {}),
    habits: types.map(Habit),
  })
  .actions((self) => ({
    addRoutine(input: SIRoutine) {
      return self.routines.put(input);
    },
    addHabit(input: SIHabit) {
      return self.habits.put(input);
    },
    updateHabit(input: SIHabit) {
      return self.habits.put(input);
    },
    removeRoutine(id: string) {
      const removed = self.routines.delete(id);
      return removed;
    },
    removeHabit(id: string) {
      const removed = self.habits.delete(id);
      return removed;
    },
  }))
  .actions((self) => ({
    loadRoutinesAndHabits: flow(function* flowFetchRoutines() {
      const allKeys: string[] = yield AsyncStorage.getAllKeys();
      const allRoutineKeys = allKeys.filter((ak) => ak.startsWith('Routine-'));
      const allHabitKeys = allKeys.filter((ak) => ak.startsWith('Habit-'));
      const routineStringList: [
        string,
        string | null
      ][] = yield AsyncStorage.multiGet(allRoutineKeys);
      const habitStringList: [
        string,
        string | null
      ][] = yield AsyncStorage.multiGet(allHabitKeys);
      routineStringList.forEach(([, routineString]) => {
        const routineModel =
          typeof routineString === 'string' && JSON.parse(routineString);
        if (routineModel) {
          self.addRoutine(routineModel);
          // console.log(`[${routineModel.id}]: Loaded`);
        }
      });
      habitStringList.forEach(([, habitString]) => {
        const habitModel =
          typeof habitString === 'string' && JSON.parse(habitString);
        if (habitModel) {
          self.addHabit(habitModel);
          // console.log(`[${habitModel.id}]: Loaded`);
        }
      });
    }),
  }));

/**
 * UNUSED --
 * TODO: think aobut relationship between session + session habit
 * We don't need saving these so we should just leave it as is for now
 */
export const SessionStore = types.model({
  sessions: types.map(Session),
  sessionHabits: types.map(SessionHabit),
});

export const UserStore = types
  .model({
    user: User,
  })
  .actions((self) => ({
    loadUserFromStorage: flow(function* flowFetchUser(userId: string) {
      self.user = cast({
        id: userId,
        applicationState: { status: 'LOADING' },
      });
      try {
        const localUserString = yield AsyncStorage.getItem(`User-${userId}`);
        const localUserObject = JSON.parse(localUserString);
        if (User.is(localUserObject)) {
          self.user = cast(localUserObject);
        } else {
          self.user.applicationState.status = 'READY';
        }
      } catch (error) {
        self.user.applicationState = {
          status: 'READY',
          error,
        };
      }
      return self.user;
    }),
    saveUser() {
      return self.user.save();
    },
  }));

export const RootStore = types
  .model({
    userStore: UserStore,
    routineStore: RoutineStore,
    sessionStore: SessionStore,
  })
  .volatile(() => ({
    syncUser: null as any,
    syncRoutines: null as any,
  }))
  .actions((self) => ({
    setSyncUser(item: any) {
      self.syncUser = item;
    },
    afterCreate() {
      self.routineStore.loadRoutinesAndHabits().then(() => {
        self.userStore.loadUserFromStorage(self.userStore.user.id);
        this.setSyncUser(
          onSnapshot(self.userStore.user, () => self.userStore.user.save())
        );
      });
    },
  }));

/** TYPES */
export type IHabit = Instance<typeof Habit>;
export type SIHabit = SnapshotIn<typeof Habit>;

export type ISessionHabit = Instance<typeof SessionHabit>;
export type SISessionHabit = SnapshotIn<typeof SessionHabit>;

export type ISession = Instance<typeof Session>;
export type SISession = SnapshotIn<typeof Session>;

export type IRoutine = Instance<typeof Routine>;
export type SIRoutine = SnapshotIn<typeof Routine>;

export type IUser = Instance<typeof User>;
export type SIUser = SnapshotIn<typeof User>;

export type IUserStore = Instance<typeof UserStore>;
export type SIUserStore = SnapshotIn<typeof UserStore>;
