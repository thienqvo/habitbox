import React from 'react';
import { RootStore } from './UserStore';
import { getSnapshot } from 'mobx-state-tree';

const DEFAULT_USER_ID = 'ANONYMOUS';
const rootStore = RootStore.create({
  userStore: {
    user: {
      id: DEFAULT_USER_ID,
      applicationState: { status: 'UNINTIALIZED' },
    },
  },
  routineStore: {
    habits: {},
  },
  sessionStore: {
    sessions: {},
    sessionHabits: {},
  },
});
// rootStore.userStore.loadUserFromStorage(DEFAULT_USER_ID).catch();
console.log(getSnapshot(rootStore.userStore.user.routines));

export const StoreContext = React.createContext(rootStore);

export const StoreProvider: React.FC = ({ children }) => {
  return (
    <StoreContext.Provider value={rootStore}>{children}</StoreContext.Provider>
  );
};
export const useStores = () => {
  return React.useContext(StoreContext);
};
