module.exports = {
  native: true,
  icon: true,
  dimensions: false,
  expandProps: 'end',
  replaceAttrValues: {
    // Undraw
    // '#6C63FF': '{props.fill}',
    // '#6C23FF': '{props.fill}',
    // Flower mix
    '#0BCEB2': '{props.secondary}',
    '#2D4356': '{props.primary}',
  },
};
