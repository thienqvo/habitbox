import React from 'react';
import { observer, useLocalStore } from 'mobx-react';
import numeral from 'numeral';

import { Text } from '../components/system/typography';
import { SafeAreaView, View, ScrollView } from '../components/system/views';
import { Input } from '../components/system/input';
import { useRoutineNavigation, useRoutineRoute } from './utils';
import { TouchableOpacity } from 'react-native';
import { Header } from '../components/Header';
import { ImageBackgroundCard } from '../components/ImageBackgroundCard';
import { getDurationText } from '../components/utils';
import { defaultTheme } from '../theme';
import { useStores } from '../stores/StoreProvider';
import { HabitImageKeys, getHabitImageUrl } from '../utils/images';

const Divider = () => <View testID={'divider'} py={4} />;

const DEFAULT_COLOR = defaultTheme.colors.night;

export const CreateHabit: React.FC = observer(() => {
  const navigator = useRoutineNavigation();
  const { params } = useRoutineRoute<'CreateHabit'>();
  const { routineStore } = useStores();
  const routine = params.routine;

  // local state
  // cbs
  const goBack = () => {
    if (navigator.canGoBack()) {
      navigator.goBack();
    } else {
      navigator.navigate('RoutineDetails', { routine });
    }
  };
  const store = useLocalStore(() => ({
    color: DEFAULT_COLOR,
    name: '',
    minute: 0,
    seconds: 0,
    imageKey: 'code',
    get totalDuration() {
      return this.minute * 60 + this.seconds;
    },
    get durationText() {
      return getDurationText(this.totalDuration);
    },
    onMinuteChange(text: string) {
      const minute = numeral(text).value();
      store.minute = Math.max(minute, 0);
    },
    onSecondsChange(text: string) {
      const seconds = numeral(text).value();
      if (seconds > 0 && seconds <= 60) {
        store.seconds = seconds;
      }
    },
    onSubmit() {
      if (store.name && store.totalDuration) {
        const habit = routineStore.addHabit({
          name: store.name,
          duration: store.totalDuration,
          color: store.color,
        });
        routine.linkHabit(habit.id);
        goBack();
      }
    },
  }));
  const canSubmit =
    !!store.name && !!store.totalDuration && store.color !== DEFAULT_COLOR;

  return (
    <SafeAreaView height={'100%'} backgroundColor={'beluga'}>
      <View
        position={'absolute'}
        height={100}
        top={-50}
        left={0}
        right={0}
        backgroundColor={store.color}
      />
      <Header
        goBack={navigator.goBack}
        backgroundColor={store.color}
        RightItem={() => (
          <TouchableOpacity
            onPress={canSubmit ? store.onSubmit : undefined}
            activeOpacity={canSubmit ? 0.5 : 1}
          >
            <Text
              size={'lg'}
              color={canSubmit ? 'cumulus' : 'cumulus-50'}
              weight={'medium'}
            >
              {'Done'}
            </Text>
          </TouchableOpacity>
        )}
      />
      <ScrollView mx={3} mt={2} showsVerticalScrollIndicator={false}>
        <View mt={5}>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Preview'}
          </Text>
          <ImageBackgroundCard
            imgSource={{ uri: getHabitImageUrl(store.imageKey, 'w') }}
            title={store.name}
            subtitle={store.durationText}
            accentColor={store.color}
            style={{ marginTop: 12 }}
          />
        </View>
        <Divider />
        <View>
          <Text size={'xs'} weight={'semibold'} color={'shilling'} mb={2}>
            {'Name & Duration'}
          </Text>
          <Input
            value={store.name}
            onChangeText={(text) => (store.name = text)}
            size={'lg'}
            width={'100%'}
            color={'cumulus'}
            placeholder={'Habit name'}
          />
          <View flexDirection={'row'} mt={4}>
            <Input
              value={store.minute ? store.minute.toString() : ''}
              onChangeText={store.onMinuteChange}
              size={'lg'}
              color={'cumulus'}
              flex={1}
              placeholder={'Minute'}
              keyboardType={'number-pad'}
            />
            <Input
              value={store.seconds ? store.seconds.toString() : ''}
              onChangeText={store.onSecondsChange}
              size={'lg'}
              color={'cumulus'}
              ml={4}
              flex={1}
              placeholder={'Seconds'}
              keyboardType={'number-pad'}
            />
          </View>
        </View>
        <Divider />
        <View>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Color'}
          </Text>
          <View flexDirection={'row'} justifyContent={'space-between'} mt={3}>
            {['#F53566', '#F8BC32', '#0EAD69', '#1D74F1', '#542FBC'].map(
              (color) => (
                <TouchableOpacity
                  key={color}
                  onPress={() => (store.color = color)}
                >
                  <View
                    width={48}
                    height={48}
                    borderRadius={24}
                    backgroundColor={color}
                    opacity={store.color === color ? 1 : 0.3}
                  />
                </TouchableOpacity>
              )
            )}
          </View>
        </View>
        <Divider />
        <View mb={4}>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Image'}
          </Text>
          <View mt={4}>
            {HabitImageKeys.map((imageName, index) => (
              <TouchableOpacity
                key={imageName}
                onPress={() => (store.imageKey = imageName)}
              >
                <ImageBackgroundCard
                  imgSource={{ uri: getHabitImageUrl(imageName, 'w') }}
                  title={''}
                  subtitle={''}
                  accentColor={'transparent'}
                  overlayOptions={{ disabled: true }}
                  bannerText={
                    store.imageKey === imageName ? 'Selected' : undefined
                  }
                  style={{
                    marginTop: index === 0 ? 0 : 16,
                    opacity: store.imageKey === imageName ? 1 : 0.5,
                  }}
                />
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});
