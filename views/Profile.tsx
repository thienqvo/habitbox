import React from 'react';
import { observer } from 'mobx-react';
import { SafeAreaView, ScrollView } from '../components/system/views';
import { Heading, Text } from '../components/system/typography';

export const Profile: React.FC = observer(() => {
  return (
    <SafeAreaView flex={1} backgroundColor={'beluga'}>
      <ScrollView flex={1} mx={3} mt={3}>
        <Heading size={'h2'} color={'cumulus'}>
          {'Profile'}
        </Heading>
        <Text color={'cumulus'} size={'lg'} mt={4}>
          {'Coming soon!'}
        </Text>
      </ScrollView>
    </SafeAreaView>
  );
});
