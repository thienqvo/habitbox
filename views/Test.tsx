import React from 'react';
import { observer } from 'mobx-react';
import { SafeAreaView, ScrollView } from '../components/system/views';
import { Heading } from '../components/system/typography';

import { Svg, Path } from 'react-native-svg';
import { Animated } from 'react-native';
import { TouchableOpacity } from 'react-native';

function polarToCartesian(
  centerX: number,
  centerY: number,
  radius: number,
  angleInDegrees: number
) {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;

  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
}

function describeArc(
  x: number,
  y: number,
  radius: number,
  startAngle: number,
  endAngle: number
) {
  const start = polarToCartesian(x, y, radius, endAngle);
  const end = polarToCartesian(x, y, radius, startAngle);

  const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';

  const d = [
    'M',
    start.x,
    start.y,
    'A',
    radius,
    radius,
    0,
    largeArcFlag,
    0,
    end.x,
    end.y,
  ].join(' ');

  return d;
}

export const Test: React.FC = observer(() => {
  const pathRef = React.useRef<any>(null);
  const arc = React.useRef(new Animated.Value(0)).current;
  const arcSpringToFull = React.useRef(
    Animated.spring(arc, {
      toValue: 358,
      stiffness: 80,
      damping: 20,
      mass: 3,
      overshootClamping: true,
      useNativeDriver: true,
    })
  ).current;
  const arcSpringFullFinishedRef = React.useRef(false);

  const arcSpringToEmpty = React.useRef(
    Animated.spring(arc, {
      toValue: 0,
      stiffness: 100,
      damping: 20,
      mass: 3,
      overshootClamping: true,
      useNativeDriver: true,
    })
  ).current;

  const startAnimation = () => {
    arcSpringToFull.start(({ finished }) => {
      console.log(finished);
      arcSpringFullFinishedRef.current = finished;
    });
  };

  const resetAnimation = () => {
    if (!arcSpringFullFinishedRef.current) {
      arcSpringToFull.reset();
      arcSpringToEmpty.start();
    }
  };

  React.useEffect(() => {
    arc.addListener(({ value }) => {
      pathRef.current?.setNativeProps({
        d: describeArc(175, 175, 50, 0, value),
        opacity: value === 0 ? 0 : 1,
      });
    });
  }, []);

  return (
    <SafeAreaView flex={1} backgroundColor={'beluga'}>
      <ScrollView flex={1} mx={3} mt={3}>
        <Heading size={'h2'} color={'cumulus'}>
          {'Profile'}
        </Heading>
        <TouchableOpacity
          onLongPress={startAnimation}
          onPressOut={resetAnimation}
          activeOpacity={1}
        >
          <Svg width={400} height={400}>
            <Path
              d={describeArc(175, 175, 50, 0, 0)}
              opacity={0}
              strokeWidth={5}
              stroke={'white'}
              strokeLinecap={'round'}
              ref={(r) => {
                pathRef.current = r;
              }}
            />
          </Svg>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
});
