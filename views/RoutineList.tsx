import React from 'react';
import { observer } from 'mobx-react';

import { Heading } from '../components/system/typography';
import { SafeAreaView, ScrollView, View } from '../components/system/views';
import { RoutineRow } from '../components/RoutineRow';
import { useStores } from '../stores/StoreProvider';
import { useRoutineNavigation } from './utils';
import { IonIcon } from '../components/system/IonIcon';
import { useTheme } from 'styled-components';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IRoutine } from '../stores/UserStore';

export const RoutineList: React.FC = observer(() => {
  const { userStore } = useStores();
  const navigator = useRoutineNavigation();
  const theme = useTheme();

  const onSelectRoutine = (routine: IRoutine) => {
    navigator.navigate('HabitList', { routine });
  };

  const onCreateRoutinePress = () => {
    navigator.navigate('CreateRoutine');
  };

  return (
    <SafeAreaView flex={1} backgroundColor={'beluga'}>
      <ScrollView
        flex={1}
        px={4}
        py={4}
        showsVerticalScrollIndicator={false}
        bounces={false}
      >
        <View
          flexDirection={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
        >
          <Heading size={'h2'} color={'cumulus'}>
            {'Your routines'}
          </Heading>
          <TouchableOpacity onPress={onCreateRoutinePress}>
            <View
              backgroundColor={'possum'}
              width={40}
              height={40}
              flexShrink={0}
              borderRadius={4}
              justifyContent={'center'}
              alignItems={'center'}
            >
              <IonIcon
                name={'add-outline'}
                size={28}
                color={theme.colors.white}
              />
            </View>
          </TouchableOpacity>
        </View>
        {userStore.user.routines.map((item) => (
          <RoutineRow
            key={item.id}
            routine={item}
            onPress={() => onSelectRoutine(item)}
            onStart={(routine) =>
              navigator.navigate('FullscreenSession', {
                session: routine.toSession(),
              })
            }
            style={{ marginTop: 24 }}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
});
