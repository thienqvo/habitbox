import React from 'react';
import { observer } from 'mobx-react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Platform } from 'react-native';

import { CreateRoutine } from './CreateRoutine';
import { RoutineList } from './RoutineList';
import { HabitList } from './HabitList';
import { CreateHabit } from './CreateHabit';
import { RoutineScreenStackParams, HomeScreenTabParams } from './utils';
import { FullscreenSession } from './FullscreenSession';
import { IonIcon } from '../components/system/IonIcon';
import { Profile } from './Profile';
import { SessionSummary } from './SessionSummary';
import { HabitDetails } from './HabitDetails';
import { RoutineDetails } from './RoutineDetails';

const Stack = createStackNavigator<RoutineScreenStackParams>();
const BottomTab = createBottomTabNavigator<HomeScreenTabParams>();

export const RoutineListScreen = observer(() => (
  <BottomTab.Navigator
    tabBarOptions={{
      labelStyle: {
        fontFamily: 'SFProText-SemiBold',
        fontSize: 14,
      },
      tabStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#353535',
      },
      activeTintColor: '#F3F3F3',
      inactiveTintColor: '#A3A3A3',
    }}
  >
    <BottomTab.Screen
      name="Routines"
      component={RoutineList}
      options={{
        tabBarIcon: ({ focused, color, size }) => {
          return (
            <IonIcon
              name="list"
              color={focused ? color : 'shilling'}
              style={{ height: size, width: size, fontSize: size }}
            />
          );
        },
      }}
    />
    <BottomTab.Screen
      name="Profile"
      component={Profile}
      options={{
        tabBarIcon: ({ focused, color, size }) => (
          <IonIcon
            name="settings-outline"
            color={focused ? color : 'shilling'}
            style={{ height: size, width: size, fontSize: size }}
          />
        ),
      }}
    />
  </BottomTab.Navigator>
));

export const AppScreens: React.FC = observer(() => {
  return (
    <Stack.Navigator
      headerMode={'none'}
      initialRouteName={'RoutineList'}
      screenOptions={{
        animationEnabled: Platform.select({ ios: true, android: false }),
      }}
    >
      <Stack.Screen name={'RoutineList'} component={RoutineListScreen} />
      <Stack.Screen
        name={'CreateRoutine'}
        component={CreateRoutine}
        options={{ gestureEnabled: false }}
      />
      <Stack.Screen name={'HabitList'} component={HabitList} />
      <Stack.Screen
        name={'CreateHabit'}
        component={CreateHabit}
        options={{ gestureEnabled: false }}
      />
      <Stack.Screen name={'FullscreenSession'} component={FullscreenSession} />
      <Stack.Screen name={'SessionSummary'} component={SessionSummary} />
      <Stack.Screen name={'HabitDetails'} component={HabitDetails} />
      <Stack.Screen name={'RoutineDetails'} component={RoutineDetails} />
    </Stack.Navigator>
  );
});
