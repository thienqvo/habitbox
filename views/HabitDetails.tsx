import React from 'react';
import { observer, useLocalStore } from 'mobx-react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  ScrollView,
} from '../components/system/views';
import { useRoutineRoute, useRoutineNavigation } from './utils';
import { Header } from '../components/Header';
import { ImageBackgroundCard } from '../components/ImageBackgroundCard';
import { Input } from '../components/system/input';
import numeral from 'numeral';
import { Text } from '../components/system/typography';
import { HabitImageKeys, getHabitImageUrl } from '../utils/images';
import { IonIcon } from '../components/system/IonIcon';
import { useStores } from '../stores/StoreProvider';

const Divider = () => <View testID={'divider'} py={4} />;

export const HabitDetails: React.FC = observer(() => {
  const navigator = useRoutineNavigation();
  const { routineStore } = useStores();
  const { params } = useRoutineRoute<'HabitDetails'>();

  const { habit, routine } = params;

  const store = useLocalStore(() => ({
    name: habit.name,
    minutes: habit.duration && Math.floor(habit.duration / 60),
    seconds: habit.duration % 60,
    get totalDuration() {
      return this.minutes * 60 + this.seconds;
    },
  }));

  return (
    <SafeAreaView flex={1} backgroundColor={'beluga'}>
      <Header
        goBack={navigator.goBack}
        backgroundColor={habit.color}
        RightItem={() => (
          <TouchableOpacity
            onPress={() => {
              routine.unlinkHabit(habit.id);
              routineStore.removeHabit(habit.id);
              navigator.goBack();
            }}
          >
            <IonIcon size={28} name={'trash'} color={'cumulus'} />
          </TouchableOpacity>
        )}
      />
      <ScrollView mx={3} mt={2} showsVerticalScrollIndicator={false}>
        <View mt={5}>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Preview'}
          </Text>
          <ImageBackgroundCard
            imgSource={{ uri: getHabitImageUrl(habit.imageKey, 'w') }}
            title={habit.name}
            subtitle={habit.durationText}
            accentColor={habit.color}
            style={{ marginTop: 12 }}
          />
        </View>
        <Divider />
        <View>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Name & Duration'}
          </Text>
          <Input
            value={store.name}
            onChangeText={(text) => (store.name = text)}
            onBlur={() => habit.setName(store.name)}
            size={'lg'}
            width={'100%'}
            color={'cumulus'}
            placeholder={'Habit name'}
          />
          <View flexDirection={'row'} mt={4}>
            <Input
              value={store.minutes ? store.minutes.toString() : ''}
              onChangeText={(text) => {
                store.minutes = Math.max(numeral(text).value(), 0);
              }}
              onBlur={() => habit.setDuration(store.totalDuration)}
              size={'lg'}
              color={'cumulus'}
              flex={1}
              placeholder={'Minute'}
              keyboardType={'number-pad'}
            />
            <Input
              value={store.seconds ? store.seconds.toString() : ''}
              onChangeText={(text) => {
                store.seconds = Math.max(numeral(text).value(), 0);
              }}
              onBlur={() => habit.setDuration(store.totalDuration)}
              size={'lg'}
              color={'cumulus'}
              ml={4}
              flex={1}
              placeholder={'Seconds'}
              keyboardType={'number-pad'}
            />
          </View>
        </View>
        <Divider />
        <View>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Color'}
          </Text>
          <View flexDirection={'row'} justifyContent={'space-between'} mt={3}>
            {['#F53566', '#F8BC32', '#0EAD69', '#1D74F1', '#542FBC'].map(
              (color) => (
                <TouchableOpacity
                  key={color}
                  onPress={() => habit.setColor(color)}
                >
                  <View
                    width={48}
                    height={48}
                    borderRadius={24}
                    backgroundColor={color}
                    opacity={habit.color === color ? 1 : 0.3}
                  />
                </TouchableOpacity>
              )
            )}
          </View>
        </View>
        <Divider />
        <View mb={4}>
          <Text size={'xs'} weight={'semibold'} color={'shilling'}>
            {'Image'}
          </Text>
          <View mt={4}>
            {HabitImageKeys.map((imageName, index) => (
              <TouchableOpacity
                key={imageName}
                onPress={() => habit.setImageKey(imageName)}
              >
                <ImageBackgroundCard
                  imgSource={{ uri: getHabitImageUrl(imageName, 'w') }}
                  title={''}
                  subtitle={''}
                  accentColor={'transparent'}
                  overlayOptions={{ disabled: true }}
                  bannerText={
                    habit.imageKey === imageName ? 'Selected' : undefined
                  }
                  style={{
                    marginTop: index === 0 ? 0 : 16,
                    opacity: habit.imageKey === imageName ? 1 : 0.5,
                  }}
                />
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});
