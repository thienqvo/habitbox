import React from 'react';
import { observer, useLocalStore } from 'mobx-react';
import Color from 'color';

import { Text } from '../components/system/typography';
import {
  SafeAreaView,
  View,
  ScrollView,
  TouchableOpacity,
} from '../components/system/views';
import { useStores } from '../stores/StoreProvider';
import { Input } from '../components/system/input';
import { useRoutineNavigation } from './utils';
import {
  IllustrationVectors,
  IllustrationNames,
} from '../components/IllustrationVectors';
import Carousel from 'react-native-snap-carousel';
import { useTheme } from 'styled-components';
import { Header } from '../components/Header';

export const CreateRoutine: React.FC = observer(() => {
  const { userStore, routineStore } = useStores();
  const navigator = useRoutineNavigation();
  const theme = useTheme();

  const store = useLocalStore(() => ({
    routineName: '',
    primaryColor: theme.colors.wintersky,
    illustrationName: 'Flower01' as IllustrationNames,
    get secondaryColor() {
      return Color(this.primaryColor).saturate(-0.5).lighten(0.5).toString();
    },
  }));

  const goBack = () => {
    if (navigator.canGoBack()) {
      navigator.goBack();
    } else {
      navigator.navigate('RoutineList');
    }
  };
  const onSubmit = () => {
    const title =
      store.routineName || `My Routine #${userStore.user.routines.length + 1}`;
    const routine = routineStore.addRoutine({
      title,
      displayInfo: {
        color: store.primaryColor,
        illustrationName: store.illustrationName,
      },
    });
    userStore.user.linkRoutine(routine.id);
    navigator.replace('HabitList', { routine });
  };

  const HEADER_BACKGROUND_COLOR = '#4E4E53';
  const HEADER_HEIGHT = 50;

  const canSubmit = store.routineName.length > 0;

  return (
    <SafeAreaView flex={1} backgroundColor={'beluga'}>
      <View
        position={'absolute'}
        height={HEADER_HEIGHT}
        top={-HEADER_HEIGHT}
        left={0}
        right={0}
        backgroundColor={HEADER_BACKGROUND_COLOR}
      />
      <Header
        goBack={goBack}
        RightItem={() => (
          <TouchableOpacity onPress={canSubmit ? onSubmit : undefined}>
            <Text
              size={'lg'}
              color={canSubmit ? 'cumulus' : 'cumulus-50'}
              weight={'medium'}
            >
              {'Done'}
            </Text>
          </TouchableOpacity>
        )}
      />
      <ScrollView flex={1} stickyHeaderIndices={[0]} bounces={false}>
        <View mt={7} px={8} width={'100%'}>
          <Carousel
            data={IllustrationNames}
            renderItem={({ item }) => {
              return (
                <IllustrationVectors
                  name={item}
                  primary={store.primaryColor}
                  secondary={store.secondaryColor}
                  width={200}
                  height={200}
                />
              );
            }}
            sliderWidth={300}
            itemWidth={200}
            inactiveSlideOpacity={0.5}
            inactiveSlideScale={0.8}
            onSnapToItem={(index) =>
              (store.illustrationName = IllustrationNames[index])
            }
          />
          <View
            marginY={6}
            flexDirection={'row'}
            justifyContent={'space-between'}
          >
            {['wintersky', 'mango', 'gogreen', 'skydive', 'blueviolet']
              .map((colorName) => theme.colors[colorName])
              .map((color) => (
                <TouchableOpacity
                  key={color}
                  onPress={() => (store.primaryColor = color)}
                >
                  <View
                    height={48}
                    width={48}
                    borderRadius={50}
                    backgroundColor={color}
                    opacity={store.primaryColor === color ? 1 : 0.3}
                  />
                </TouchableOpacity>
              ))}
          </View>
          <Input
            value={store.routineName}
            onChangeText={(text) => (store.routineName = text)}
            onSubmitEditing={onSubmit}
            size={'lg'}
            textAlign={'center'}
            width={'100%'}
            color={'cumulus'}
            returnKeyType={'done'}
            placeholder={'Routine name'}
            enablesReturnKeyAutomatically
            autoFocus
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});
