import React from 'react';
import { observer } from 'mobx-react';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
} from '../components/system/views';
import { Heading, Text } from '../components/system/typography';
import { useRoutineNavigation, useRoutineRoute } from './utils';
import { getDurationText } from '../components/utils';

export const SessionSummary: React.FC = observer(() => {
  const navigator = useRoutineNavigation();
  const { params } = useRoutineRoute<'SessionSummary'>();
  const { session } = params;

  const goBack = () => {
    navigator.canGoBack()
      ? navigator.goBack()
      : navigator.navigate('RoutineList');
  };

  const beginTime =
    session.metadata.startedTimestamp &&
    new Date(session.metadata.startedTimestamp);
  const endTime =
    session.metadata.endTimestamp && new Date(session.metadata.endTimestamp);

  const timeData =
    beginTime &&
    endTime &&
    `From ${beginTime.getHours()}:${beginTime.getMinutes()} to ${endTime.getHours()}:${endTime.getMinutes()}`;
  const timeSpentData = `Time spent: ${getDurationText(
    session.allTotalElapsedTime
  )} / ${getDurationText(session.allTotalDuration)}`;

  return (
    <SafeAreaView backgroundColor={'beluga'} flex={1}>
      <View p={4} alignItems={'flex-start'}>
        <Heading size={'h1'} style={{ marginBottom: 12 }}>
          {'Quick summary'}
        </Heading>
        {!!timeData && <Text color={'cumulus'}>{timeData}</Text>}
        <Text color={'cumulus'} mt={3}>
          {timeSpentData}
        </Text>
        <Text color={'cumulus'} mt={3}>
          {`Completed ${session.completedOrSkippedHabits.length} / ${session.habitQueue.length}`}
        </Text>
        <TouchableOpacity
          onPress={goBack}
          py={3}
          px={5}
          backgroundColor={'skydive'}
          borderRadius={6}
          mt={6}
        >
          <Text color={'cumulus'} weight={'medium'}>
            {'Done'}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
});
