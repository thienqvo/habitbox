import { useNavigation, useRoute, RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { IRoutine, ISession, IHabit } from '../stores/UserStore';

export const useRoutineNavigation = () => {
  const navigator = useNavigation<RoutineScreenStackNavigator>();
  return navigator;
};

export const useRoutineRoute = <
  T extends keyof RoutineScreenStackParams
>(): RouteProp<RoutineScreenStackParams, T> => {
  return useRoute() as any;
};

// -TYPES-
export type HomeScreenTabParams = {
  Routines: undefined;
  Profile: undefined;
  Test: undefined;
};

export type RoutineScreenStackParams = {
  RoutineList: undefined;
  CreateRoutine: undefined;
  HabitList: { routine: IRoutine };
  CreateHabit: { routine: IRoutine };
  FullscreenSession: { session: ISession };
  SessionSummary: { session: ISession };
  HabitDetails: { habit: IHabit; routine: IRoutine };
  RoutineDetails: { routine: IRoutine };
};

export type RoutineScreenStackNavigator = StackNavigationProp<
  RoutineScreenStackParams
>;
