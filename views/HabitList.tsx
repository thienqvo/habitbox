import React from 'react';
import { observer, Observer } from 'mobx-react';
import { useTheme } from 'styled-components';
import DraggableFlatList from 'react-native-draggable-flatlist';

import { Heading, Text } from '../components/system/typography';
import {
  SafeAreaView,
  View,
  TouchableOpacity,
} from '../components/system/views';
import { useRoutineNavigation, useRoutineRoute } from './utils';
import { IHabit } from '../stores/UserStore';
import { Animated } from 'react-native';
import {
  IllustrationVectors,
  IllustrationNames,
} from '../components/IllustrationVectors';
import HbIcon from '../components/HbIcons';
import { IonIcon } from '../components/system/IonIcon';
import { Header } from '../components/Header';
import { HabitCard } from '../components/HabitCard';

const BLOB_HEIGHT = 150;

export const HabitList: React.FC = observer(() => {
  const navigator = useRoutineNavigation();
  const { params } = useRoutineRoute<'HabitList'>();
  const theme = useTheme();
  const scrollY = React.useRef(new Animated.Value(0)).current;

  // derived
  const { routine } = params;
  const scaleTransition = React.useRef(new Animated.Value(0)).current;

  // callbacks
  const goBack = () => {
    navigator.canGoBack()
      ? navigator.goBack()
      : navigator.navigate('RoutineList');
  };
  const onAddHabitClick = () => {
    if (routine) {
      navigator.navigate('CreateHabit', { routine });
    }
  };

  if (!routine) {
    return (
      <SafeAreaView height={'100%'} backgroundColor={'beluga'}>
        {'Something went wrong. This routine might not exist any longer.'}
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView
      height={'100%'}
      backgroundColor={'beluga'}
      position={'relative'}
    >
      <View
        position={'absolute'}
        height={100}
        top={-50}
        left={0}
        right={0}
        backgroundColor={routine.displayInfo.secondaryColor}
      />
      <Header
        goBack={goBack}
        RightItem={() => (
          <TouchableOpacity
            onPress={() => {
              navigator.navigate('RoutineDetails', { routine });
            }}
          >
            <IonIcon size={28} name={'settings-outline'} color={'cumulus'} />
          </TouchableOpacity>
        )}
      />
      <DraggableFlatList
        // HEADER
        ListHeaderComponent={() => {
          return (
            <View alignItems={'center'} justifyContent={'flex-end'} my={4}>
              {routine.habits.length > 0 && (
                <Animated.View
                  style={{
                    height: BLOB_HEIGHT,
                    width: BLOB_HEIGHT,
                    borderRadius: 4,
                    marginBottom: 12,
                    transform: [
                      {
                        scale: scrollY.interpolate({
                          inputRange: [-50, 0, 50],
                          outputRange: [1.1, 1, 0.9],
                        }),
                      },
                    ],
                  }}
                >
                  <IllustrationVectors
                    name={
                      routine.displayInfo.illustrationName as IllustrationNames
                    }
                    width={'100%'}
                    height={'100%'}
                    primary={routine.displayInfo.primaryColor}
                    secondary={routine.displayInfo.secondaryColor}
                  />
                </Animated.View>
              )}
              <Heading
                size={'h2'}
                color={'cumulus'}
                style={{ marginBottom: 12 }}
              >
                {routine.title}
              </Heading>
              {routine.habits.length > 0 && (
                <TouchableOpacity
                  onPress={() => {
                    navigator.navigate('FullscreenSession', {
                      session: routine.toSession(),
                    });
                  }}
                >
                  <View
                    px={6}
                    py={2}
                    borderRadius={25}
                    backgroundColor={routine.displayInfo.primaryColor}
                    width={'auto'}
                  >
                    <Text color={'white'} weight={'bold'} size={'sm'}>
                      {'Start'}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
          );
        }}
        // BODY
        data={routine.habits.slice()}
        renderItem={({ item, drag, isActive }) => {
          return (
            <Observer>
              {() => (
                <View mt={3} px={3}>
                  <TouchableOpacity
                    onLongPress={drag}
                    activeOpacity={1}
                    onPress={() =>
                      navigator.navigate('HabitDetails', {
                        habit: item,
                        routine,
                      })
                    }
                  >
                    <Animated.View
                      style={{
                        transform: [
                          {
                            scale: isActive
                              ? scaleTransition.interpolate({
                                  inputRange: [0, 1],
                                  outputRange: [1, 1.05],
                                })
                              : scaleTransition.interpolate({
                                  inputRange: [0, 1],
                                  outputRange: [1, 1],
                                }),
                          },
                        ],
                      }}
                    >
                      <HabitCard habit={item} />
                    </Animated.View>
                  </TouchableOpacity>
                </View>
              )}
            </Observer>
          );
        }}
        // EMPTY STATE
        ListEmptyComponent={() => (
          <View justifyContent={'center'} alignItems={'center'}>
            <HbIcon
              name={'cat-doll-kitty'}
              width={120}
              height={120}
              style={{ flexShrink: 0 }}
            />
            <Text size={'lg'} weight={'medium'} color={'cumulus'} ml={2}>
              It's empty
            </Text>
            <IonIcon
              name={'arrow-down'}
              color={theme.colors.cumulus}
              size={40}
              style={{ marginLeft: 8 }}
            />
            <View ml={2} width={'100%'} alignItems={'center'}>
              <TouchableOpacity onPress={onAddHabitClick}>
                <View
                  px={4}
                  py={2}
                  backgroundColor={routine.displayInfo.primaryColor}
                  borderRadius={8}
                >
                  <Text size={'md'} color={'cumulus'} weight={'semibold'}>
                    {'Add habit'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
        // FOOTER
        // OTHER CONAINTER
        contentContainerStyle={{
          backgroundColor: theme.colors.beluga,
          paddingBottom: 16,
        }}
        onDragBegin={() => {
          Animated.timing(scaleTransition, {
            duration: 200,
            toValue: 1,
            useNativeDriver: false,
          }).start();
        }}
        onRelease={() => {
          Animated.timing(scaleTransition, {
            duration: 200,
            toValue: 0,
            useNativeDriver: false,
          }).start();
        }}
        keyExtractor={(item) => `draggable-item-${item.id}`}
        onDragEnd={({ data }) => {
          routine.updateHabits(data as IHabit[]);
        }}
        scrollEventThrottle={2}
        onScrollOffsetChange={(offset) => {
          scrollY.setValue(offset);
        }}
      />
      {routine.habits.length > 0 && (
        <View
          position={'absolute'}
          bottom={24}
          right={24}
          mt={6}
          width={'100%'}
          alignItems={'flex-end'}
        >
          <TouchableOpacity onPress={onAddHabitClick}>
            <View
              width={40}
              height={40}
              backgroundColor={'possum'}
              alignItems={'center'}
              justifyContent={'center'}
              borderRadius={4}
            >
              <IonIcon name={'add-outline'} size={28} color={'cumulus'} />
            </View>
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
});
