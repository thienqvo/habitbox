import React from 'react';
import { observer } from 'mobx-react';

import { SafeAreaView, View, ScrollView } from '../components/system/views';
import { useRoutineRoute, useRoutineNavigation } from './utils';
import { Text } from '../components/system/typography';
import { Header } from '../components/Header';
import { ActiveHabitTimer } from '../components/ActiveHabitTimer';
import { getDurationText } from '../components/utils';
import { SessionHabitList } from '../components/SessionHabitList';

export const FullscreenSession: React.FC = observer(() => {
  const navigator = useRoutineNavigation();
  const { params } = useRoutineRoute<'FullscreenSession'>();
  const { session } = params;

  React.useEffect(() => {
    session.begin();
    return () => {
      session.end();
    };
  }, [session.id]);

  if (!session.currentHabit) {
    return (
      <SafeAreaView>
        <Text>{'No current habit'}</Text>
      </SafeAreaView>
    );
  }

  const currentHabit = session.currentHabit;
  const goBack = () => {
    navigator.canGoBack()
      ? navigator.goBack()
      : navigator.navigate('RoutineList');
  };

  const nextHabit = session.upNextHabits[0];

  const onDurationChange = (id: string) => {
    if (
      session.currentHabit &&
      session.currentHabit.id === id &&
      !session.isPaused
    ) {
      session.currentHabit.tick();
      if (
        session.currentHabit.elapsedTime === session.currentHabit.totalDuration
      ) {
        // COMPLETION
        if (session.upNextHabits.length) {
          session.goToNextHabit('COMPLETED');
        } else {
          session.currentHabit.setStatus('COMPLETED');
          navigator.replace('SessionSummary', { session });
        }
      }
    }
  };

  const onCompletePress = (id: string) => {
    if (session.currentHabit && session.currentHabit.id === id) {
      if (session.upNextHabits.length) {
        session.goToNextHabit('SKIPPED');
      } else {
        session.currentHabit.setStatus('SKIPPED');
        navigator.replace('SessionSummary', { session });
      }
    }
  };

  return (
    <SafeAreaView backgroundColor={'beluga'} flex={1}>
      <View>
        <Header goBack={goBack} backgroundColor={currentHabit.accentColor} />
      </View>
      <ScrollView testID={'container'} flex={1} bounces={false}>
        <ActiveHabitTimer
          timerId={currentHabit.id}
          status={session.status}
          title={currentHabit.displayName}
          progressPercentage={currentHabit.progressPercentage}
          accentColor={currentHabit.accentColor}
          imageKey={currentHabit.displayImageKey}
          remainDuration={currentHabit.remainDuration}
          onAddMoreTimePress={session.addMoreTime}
          onDurationChange={(_1, _2, timerId) => onDurationChange(timerId)}
          onPause={session.pause}
          onUnpause={session.unpause}
          onCompletePress={(_1, timerId) => onCompletePress(timerId)}
        />
        <View
          testID={'up-next-container'}
          width={'100%'}
          px={4}
          mt={2}
          alignItems={'flex-end'}
        >
          <Text color={'cumulus'} weight={'semibold'}>
            {'Up next'}
          </Text>
          <View
            backgroundColor={nextHabit ? nextHabit.accentColor : 'shilling'}
            width={120}
            height={6}
            mt={2}
            borderTopLeftRadius={6}
            borderBottomLeftRadius={6}
          />
          <Text color={'cumulus'} weight={'medium'} mt={2}>
            {nextHabit ? nextHabit.displayName : 'This is the last one!'}
          </Text>
          {!!nextHabit && (
            <Text color={'cumulus'} mt={1}>
              {getDurationText(nextHabit.remainDuration)}
            </Text>
          )}
        </View>
        <View testID={'session-habit-list'} px={6} my={8}>
          <SessionHabitList
            activeHabit={session.currentHabit}
            nextHabits={session.upNextHabits}
            completedHabits={session.completedOrSkippedHabits.slice().reverse()}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
});
