export const AllHabitImageKeys = [
  'code',
  'coffee',
  'diary',
  'eat',
  'files',
  'hola',
  'internet',
  'journal',
  'meditate',
  'phone',
  'pray',
  'read',
  'run',
  'sunrise',
  'sunset',
  'swim',
  'water',
  'weights',
  'write',
  'yoga',
  'zen',
] as const;
export type HabitImageKey = typeof AllHabitImageKeys[number];
export const HabitImageKeys = (AllHabitImageKeys as any) as string[];

export const getHabitImageUrl = (key: string, resolution: 'w' | 's') => {
  if (HabitImageKeys.includes(key)) {
    return `https://res.cloudinary.com/drille/image/upload/habit-images/${key}-${resolution}.jpg`;
  } else {
    return `https://res.cloudinary.com/drille/image/upload/habit-images/code-${resolution}.jpg`;
  }
};
