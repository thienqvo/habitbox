import fs from 'fs';
import path from 'path';
import { capitalize, zip, replace } from 'lodash';

/**
 * UTILS
 */
const genComponentName = (parts: string[]) => {
  return parts.map(capitalize).join('');
};
const genGlyphName = (parts: string[]) => parts.join('-');

const genLookupTable = (pairs: [string, string]) => {
  const table: Record<string, string> = {};
  pairs.forEach(([glyphName, componentName]) => {
    table[glyphName] = componentName;
  });
  return table;
};

const genImportLine = (filename: string, componentName: string) =>
  `import ${componentName} from '${RELATIVE_PATH}/${filename}';`;

const genExportLine = (fileName: string, componentName: string) =>
  `export { default as ${componentName} } from '${RELATIVE_PATH}/${fileName}';`;

const genUnionTypeNames = (glyphNames: string[]) =>
  glyphNames.map((name) => `'${name}'`).join(' | ');

const genLookupTableTemplate = (table: Record<string, string>) => {
  return replace(
    replace(JSON.stringify(table, null, '  '), /"(?![a-z:])/g, ''),
    /"/g,
    "'"
  );
};

/** END UTILS */

const RELATIVE_PATH = path.relative(
  __dirname + '/../components',
  __dirname + '/../icons'
);
const iconDirectoryPath = path.resolve(__dirname, '../icons');
const iconPaths = fs.readdirSync(iconDirectoryPath);

const allParts = iconPaths.map((name) => name.replace('.svg', '').split('-'));

const allComponentNames = allParts.map(genComponentName);

/**
 * export as { default as ComponentName } from 'path'
 * */
const pathAndNameList = zip(iconPaths, allComponentNames);

const importLines = pathAndNameList.map(
  (item) => item[0] && item[1] && genImportLine(item[0], item[1])
);
const exportLines = pathAndNameList.map(
  (item) => item[0] && item[1] && genExportLine(item[0], item[1])
);
const HbIconsImportLines = importLines.join('\n');
const HbIconsExportDefaultLines = exportLines.join('\n');

/**
 * type HbIconsName = 'svg-glyph-name' | 'svg-glyph-name-2';
 */
const HB_ICON_NAME_TYPE_NAME = 'HbIconsNames';
const HB_ICON_NAME_CONST = 'HbIconNamesList';
const allGlyphNames = allParts.map(genGlyphName);
const unionType = genUnionTypeNames(allGlyphNames);
const HbIconsNamesConstLine = `const ${HB_ICON_NAME_CONST} = ${replace(
  JSON.stringify(allGlyphNames, null, '  '),
  /"/g,
  "'"
)}`;
const HbIconsUnionTypeLine = `type ${HB_ICON_NAME_TYPE_NAME} = ${unionType}`;
/**
 * Lookup table glyph-name: ComponentName
 */

const glyphToComponent = (zip(allGlyphNames, allComponentNames) as any) as [
  string,
  string
];
const glyphToComponentTable = genLookupTable(glyphToComponent);
const glyphToComponentTableTemplate = genLookupTableTemplate(
  glyphToComponentTable
);
/**
 * component
 */
const HbIconFile = `
import React from 'react';
import { observer } from 'mobx-react';
import { SvgProps } from 'react-native-svg';

${HbIconsImportLines}

interface HbIconProps extends SvgProps {
  name: ${HB_ICON_NAME_TYPE_NAME};
}

const HbIconLookupTable: Record<${HB_ICON_NAME_TYPE_NAME}, React.FC<SvgProps>> = ${glyphToComponentTableTemplate};

export const HbIcon: React.FC<HbIconProps> = observer((props) => {
  const { name, ...otherProps } = props;
  const Component = HbIconLookupTable[name];
  return <Component {...otherProps} />;
});

export ${HbIconsNamesConstLine};
export ${HbIconsUnionTypeLine};


export default HbIcon;
${HbIconsExportDefaultLines}
`;

fs.writeFileSync(__dirname + '/../components/HbIcons.tsx', HbIconFile);
