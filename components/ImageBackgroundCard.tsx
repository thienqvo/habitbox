import React from 'react';
import { observer } from 'mobx-react';
import { ViewStyle } from 'react-native';
import { Source } from 'react-native-fast-image';

import { View } from './system/views';
import { Text } from './system/typography';
import { IonIcon } from './system/IonIcon';
import FastImageBg from './system/ImageBackground';

interface ImageBackgroundCardProps {
  imgSource: Source;
  title: string;
  subtitle: string;
  accentColor: string;
  rightIonIconName?: IonIconNames;
  bannerText?: string;
  overlayOptions?: {
    disabled?: boolean;
  };
  style?: ViewStyle;
}

export const ImageBackgroundCard: React.FC<ImageBackgroundCardProps> = observer(
  (props) => {
    const {
      imgSource,
      title,
      subtitle,
      accentColor,
      rightIonIconName,
      bannerText,
      overlayOptions = {},
      style = {},
    } = props;
    const { opacity, ...restStyle } = style;
    return (
      <FastImageBg
        source={imgSource}
        resizeMode={'cover'}
        style={{
          ...restStyle,
          overflow: 'hidden',
          padding: 12,
          borderRadius: 6,
          minHeight: 80,
          opacity,
        }}
      >
        {!overlayOptions.disabled && (
          <View
            position={'absolute'}
            backgroundColor={'night-80'}
            top={0}
            right={0}
            left={0}
            bottom={0}
          />
        )}
        {!!bannerText && (
          <View
            position={'absolute'}
            mt={3}
            mr={3}
            top={0}
            right={0}
            px={3}
            py={1}
            backgroundColor={'black-75'}
            borderRadius={24}
          >
            <Text color={'cumulus'} weight={'semibold'} size={'xs'}>
              {bannerText}
            </Text>
          </View>
        )}
        <View
          flexDirection={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
        >
          <View>
            <View
              borderTopRightRadius={6}
              borderBottomRightRadius={6}
              backgroundColor={accentColor}
              width={120}
              height={6}
            />
            <Text color={'cumulus'} mt={2} weight={'medium'}>
              {title}
            </Text>
            <Text color={'cumulus'} mt={1}>
              {subtitle}
            </Text>
          </View>
          {!!rightIonIconName && (
            <View flexShrink={0}>
              <IonIcon name={rightIonIconName} size={28} color={'shilling'} />
            </View>
          )}
        </View>
      </FastImageBg>
    );
  }
);
