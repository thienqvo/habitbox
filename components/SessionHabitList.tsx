import React from 'react';
import { observer } from 'mobx-react';

import { ISessionHabit } from '../stores/UserStore';
import { View } from './system/views';
import { SessionHabitCard } from './SessionHabitCard';
import { Text } from './system/typography';

interface SessionHabitListProps {
  activeHabit: ISessionHabit;
  nextHabits: ISessionHabit[];
  completedHabits: ISessionHabit[];
}

export const SessionHabitList: React.FC<SessionHabitListProps> = observer(
  (props) => {
    const { activeHabit, nextHabits, completedHabits } = props;
    return (
      <View testID={'session-habit-list'}>
        {<SessionHabitCard sessionHabit={activeHabit} />}
        {nextHabits.map((sessionHabit) => (
          <SessionHabitCard
            key={sessionHabit.id}
            sessionHabit={sessionHabit}
            style={{ marginTop: 16 }}
          />
        ))}
        {completedHabits.length > 0 && (
          <View>
            <View
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              my={4}
            >
              <View
                backgroundColor={'shilling'}
                borderTopRightRadius={6}
                borderBottomRightRadius={6}
                flex={1}
                height={2}
              />
              <Text color={'shilling'} weight={'medium'} mx={4}>
                {'Completed'}
              </Text>
              <View
                backgroundColor={'shilling'}
                borderTopLeftRadius={6}
                borderBottomLeftRadius={6}
                flex={1}
                height={2}
              />
            </View>
            {completedHabits.map((sessionHabit, index) => (
              <SessionHabitCard
                key={sessionHabit.id}
                sessionHabit={sessionHabit}
                style={{
                  marginTop: index > 0 ? 16 : 0,
                }}
              />
            ))}
          </View>
        )}
      </View>
    );
  }
);
