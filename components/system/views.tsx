import styled from 'styled-components/native';
import {
  compose,
  margin,
  flexbox,
  color,
  padding,
  layout,
  border,
  position,
  opacity,
  MarginProps,
  FlexboxProps,
  ColorProps,
  PaddingProps,
  LayoutProps,
  BorderProps,
  PositionProps,
  OpacityProps,
} from 'styled-system';

type SharedViewProps = MarginProps &
  FlexboxProps &
  Omit<ColorProps, 'bg'> &
  PaddingProps &
  LayoutProps &
  BorderProps &
  PositionProps &
  OpacityProps;
const sharedFn = compose(
  flexbox,
  margin,
  color,
  padding,
  layout,
  border,
  position,
  opacity
);

export const View = styled.View<SharedViewProps>`
  ${sharedFn}
`;

View.defaultProps = {
  width: 'auto',
  height: 'auto',
};

export const SafeAreaView = styled.SafeAreaView<SharedViewProps>`
  ${sharedFn}
`;

SafeAreaView.defaultProps = {
  width: 'auto',
  height: 'auto',
};

export const ScrollView = styled.ScrollView<SharedViewProps>`
  ${sharedFn}
`;

ScrollView.defaultProps = {
  width: 'auto',
  height: 'auto',
};

export const TouchableOpacity = styled.TouchableOpacity<SharedViewProps>`
  ${sharedFn}
`;

TouchableOpacity.defaultProps = {
  width: 'auto',
  height: 'auto',
  activeOpacity: 0.5,
};

export const ImageBackground = styled.ImageBackground<SharedViewProps>`
  ${sharedFn}
`;

const Views = {
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
};

export default Views;
