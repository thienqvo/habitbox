import styled from 'styled-components/native';
import {
  compose,
  color,
  typography,
  variant,
  system,
  layout,
  border,
  margin,
  flex,
  padding,
} from 'styled-system';
import {
  ColorProps,
  TypographyProps,
  LayoutProps,
  BorderProps,
  MarginProps,
  FlexProps,
  PaddingProps,
} from 'styled-system';
import { ThemeStyle, getHeightByFontSize, defaultTheme } from '../../theme';

interface InputProps
  extends ColorProps,
    LayoutProps,
    BorderProps,
    MarginProps,
    FlexProps,
    PaddingProps,
    Omit<TypographyProps, 'fontFamily' | 'fontSize' | 'letterSpacing'> {
  size?: 'sm' | 'md' | 'lg';
  weight?: keyof ThemeStyle['fonts'];
}

const sharedFn = compose(
  color,
  typography,
  layout,
  border,
  margin,
  flex,
  padding
);

const inputSizeVariant: Record<
  NonNullable<InputProps['size']>,
  { fontSize: keyof ThemeStyle['fontSizes']; height: number }
> = {
  sm: {
    fontSize: 'sm',
    height: getHeightByFontSize('sm'),
  },
  md: {
    fontSize: 'md',
    height: getHeightByFontSize('md'),
  },
  lg: {
    fontSize: 'lg',
    height: getHeightByFontSize('lg'),
  },
};

export const Input = styled.TextInput<InputProps>`
  ${variant({
    prop: 'size',
    variants: inputSizeVariant,
  })}
  ${system({
    weight: {
      property: 'fontFamily',
      scale: 'fonts',
    },
  })}
  ${sharedFn}
`;

Input.defaultProps = {
  size: 'md',
  width: 'auto',
  height: 'auto',
  weight: 'regular',
  keyboardAppearance: 'dark',
  borderBottomWidth: 1,
  borderBottomColor: 'shilling-75',
  placeholderTextColor: defaultTheme.colors['cumulus-50'],
  pb: 2,
};
