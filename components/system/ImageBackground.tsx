import React from 'react';
import { observer } from 'mobx-react';
import { ViewStyle, ImageStyle } from 'react-native';
import FastImage, { FastImageProps } from 'react-native-fast-image';

import { View } from './views';
import { StyleSheet } from 'react-native';

interface ImageBackgroundProps extends Omit<FastImageProps, 'style'> {
  style?: ViewStyle;
  imageStyle?: ImageStyle;
}

class ImageBackground extends React.Component<ImageBackgroundProps> {
  _viewRef: any = null;
  setNativeProps(props: Object) {
    // Work-around flow
    const viewRef = this._viewRef;
    if (viewRef) {
      viewRef.setNativeProps(props);
    }
  }

  _captureRef = (ref: any) => {
    this._viewRef = ref;
  };

  render() {
    const { children, style, imageStyle = {}, ...props } = this.props;

    return (
      <View
        accessibilityIgnoresInvertColors={true}
        style={style}
        ref={this._captureRef}
      >
        <FastImage
          {...props}
          style={[
            StyleSheet.absoluteFill,
            {
              // Temporary Workaround:
              // Current (imperfect yet) implementation of <Image> overwrites width and height styles
              // (which is not quite correct), and these styles conflict with explicitly set styles
              // of <ImageBackground> and with our internal layout model here.
              // So, we have to proxy/reapply these styles explicitly for actual <Image> component.
              // This workaround should be removed after implementing proper support of
              // intrinsic content size of the <Image>.
              width: style?.width,
              height: style?.height,
            },
            imageStyle,
          ]}
        />
        {children}
      </View>
    );
  }
}

export const FastImageBg = observer(ImageBackground);
export default FastImageBg;
