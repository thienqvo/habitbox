import React from 'react';
import { observer } from 'mobx-react';
import { TouchableOpacity, TouchableOpacityProps } from 'react-native';

import { View } from './views';
import { Text, TextProps } from './typography';

// -SYSTEM-
const containerPropsByType: Record<HbButtonType, ViewProps> = {
  primary: {
    backgroundColor: 'skydive',
  },
  secondary: {
    backgroundColor: 'cumulus',
  },
  get default() {
    return this.primary;
  },
};

const textPropsByType: Record<HbButtonType, TextProps> = {
  primary: {
    color: 'cumulus',
  },
  secondary: {
    color: 'skydive',
  },
  get default() {
    return this.primary;
  },
};

const containerPropsBySize: Record<HbButtonSize, ViewProps> = {
  sm: {
    px: 5,
    py: 2,
    borderRadius: 6,
  },
  md: {
    px: 7,
    py: 3,
    borderRadius: 6,
  },
  lg: {
    px: 7,
    py: 3,
    borderRadius: 6,
  },
  get default() {
    return this.md;
  },
};

const textPropsBySize: Record<HbButtonSize, TextProps> = {
  sm: {
    size: 'md',
    weight: 'regular',
  },
  md: {
    size: 'lg',
    weight: 'regular',
  },
  lg: {
    size: 'xl',
    weight: 'medium',
  },
  get default() {
    return this.md;
  },
};
// end -SYSTEM-

export const Button: React.FC<ButtonProps> = observer((props) => {
  const {
    type = 'default',
    size = 'default',
    disabled,
    children,
    ...otherProps
  } = props;
  return (
    <TouchableOpacity {...otherProps} disabled={disabled}>
      <View
        {...containerPropsByType[type]}
        {...containerPropsBySize[size]}
        opacity={disabled ? 0.5 : 1}
      >
        <Text {...textPropsByType[type]} {...textPropsBySize[size]}>
          {children}
        </Text>
      </View>
    </TouchableOpacity>
  );
});

export const TextButton: React.FC<TextButtonProps> = observer((props) => {
  const {
    size = 'default',
    textOptions = {},
    disabled,
    children,
    ...otherProps
  } = props;
  return (
    <TouchableOpacity {...otherProps} disabled={disabled}>
      <View opacity={disabled ? 0.5 : 1}>
        <Text {...textPropsBySize[size]} {...textOptions}>
          {children}
        </Text>
      </View>
    </TouchableOpacity>
  );
});

// -TYPES-
type ViewProps = React.ComponentProps<typeof View>;

export type HbButtonType = 'default' | 'primary' | 'secondary';
export type HbButtonSize = 'sm' | 'md' | 'lg' | 'default';
export interface ButtonProps extends TouchableOpacityProps {
  type?: HbButtonType;
  size?: HbButtonSize;
}
export interface TextButtonProps extends TouchableOpacityProps {
  size?: HbButtonSize;
  textOptions?: Omit<TextProps, 'size' | 'weight' | 'children'>;
}
