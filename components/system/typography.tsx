import styled from 'styled-components/native';
import {
  color,
  ColorProps,
  typography,
  TypographyProps,
  system,
  variant,
  margin,
  MarginProps,
  compose,
} from 'styled-system';

import { ThemeStyle } from '../../theme';

export interface TextProps
  extends ColorProps,
    MarginProps,
    Omit<TypographyProps, 'fontFamily' | 'lineHeight' | 'letterSpacing'> {
  weight?: keyof ThemeStyle['fonts'];
  size?: keyof ThemeStyle['fontSizes'];
}

const textSizeVariants: Record<
  NonNullable<TextProps['size']>,
  { fontSize: keyof ThemeStyle['fontSizes'] }
> = {
  xs: {
    fontSize: 'xs',
  },
  sm: {
    fontSize: 'sm',
  },
  md: {
    fontSize: 'md',
  },
  lg: {
    fontSize: 'lg',
  },
  xl: {
    fontSize: 'xl',
  },
  xxl: {
    fontSize: 'xxl',
  },
  '3xl': {
    fontSize: '3xl',
  },
  '4xl': {
    fontSize: '4xl',
  },
  timerSize: {
    fontSize: 'timerSize',
  },
};

export const Text = styled.Text<TextProps>`
  ${system({
    weight: {
      property: 'fontFamily',
      scale: 'fonts',
    },
  })};
  ${variant({
    prop: 'size',
    variants: textSizeVariants,
  })}
  ${compose(color, typography, margin)}
`;

Text.defaultProps = {
  weight: 'regular',
  size: 'md',
};

interface HeadingProps extends ColorProps {
  size: 'h1' | 'h2' | 'h3';
}

const headingSizeVariants: Record<
  HeadingProps['size'],
  {
    fontSize: keyof ThemeStyle['fontSizes'];
    fontFamily: keyof ThemeStyle['fonts'];
  }
> = {
  h1: {
    fontSize: '4xl',
    fontFamily: 'bold',
  },
  h2: {
    fontSize: '3xl',
    fontFamily: 'bold',
  },
  h3: {
    fontSize: 'xxl',
    fontFamily: 'bold',
  },
};

export const Heading = styled.Text<HeadingProps>`
  ${color}
  ${variant({
    prop: 'size',
    variants: headingSizeVariants,
  })}
`;

Heading.defaultProps = {
  color: 'cumulus',
};

const Typography = { Text, Heading };
export default Typography;
