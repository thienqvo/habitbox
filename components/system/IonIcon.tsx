import React from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Ionicons';
import { color, ColorProps } from 'styled-system';

interface IonIconProps
  extends Omit<React.ComponentProps<typeof Icon>, 'color'>,
    ColorProps {
  name: IonIconNames;
}

export const IonIcon = styled(Icon)`
  ${color}
` as React.ComponentType<IonIconProps>;
