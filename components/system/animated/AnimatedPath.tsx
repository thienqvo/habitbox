import React from 'react';
import { Path, PathProps } from 'react-native-svg';
import { NativeMethods } from 'react-native';

interface PathRef
  extends React.Component<PathProps>,
    Pick<NativeMethods, 'setNativeProps'> {
  setNativeProps(props: PathProps): void;
}

export type AnimatedPathRef = PathRef | null;

export const AnimatedPath = React.forwardRef<AnimatedPathRef, PathProps>(
  (props, ref) => {
    const internalRef = React.useRef<AnimatedPathRef>(null);
    React.useImperativeHandle(ref, () => internalRef.current as PathRef);

    return (
      <Path
        {...props}
        ref={(r) => {
          internalRef.current = r as any;
        }}
      />
    );
  }
);

/**
 * Related utilities
 */
function polarToCartesian(
  centerX: number,
  centerY: number,
  radius: number,
  angleInDegrees: number
) {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;

  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
}

export function describeArc(
  x: number,
  y: number,
  radius: number,
  startAngle: number,
  endAngle: number
) {
  const start = polarToCartesian(x, y, radius, endAngle);
  const end = polarToCartesian(x, y, radius, startAngle);

  const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';

  const d = [
    'M',
    start.x,
    start.y,
    'A',
    radius,
    radius,
    0,
    largeArcFlag,
    0,
    end.x,
    end.y,
  ].join(' ');

  return d;
}
