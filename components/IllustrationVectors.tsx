import React from 'react';
import { SvgProps } from 'react-native-svg';
import { observer } from 'mobx-react';

import * as Illustrations from '../illustrations';

export type IllustrationNames = keyof typeof Illustrations;
export const IllustrationNames = Object.keys(
  Illustrations
) as IllustrationNames[];

interface IllustrationVectorProps extends Omit<SvgProps, 'fill'> {
  name: IllustrationNames;
  primary: string;
  secondary: string;
}

export const IllustrationVectors: React.FC<IllustrationVectorProps> = observer(
  ({ name, ...otherProps }) => {
    const Component = Illustrations[name];
    return <Component {...otherProps} />;
  }
);
