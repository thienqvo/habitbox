import React from 'react';
import { observer } from 'mobx-react';
import { ViewProps } from 'react-native';

import { IHabit } from '../stores/UserStore';
import { getDurationText } from './utils';
import { ImageBackgroundCard } from './ImageBackgroundCard';
import { getHabitImageUrl } from '../utils/images';

interface HabitCardProps {
  habit: IHabit;
  style?: ViewProps['style'];
}

export const HabitCard: React.FC<HabitCardProps> = observer((props) => {
  const { habit } = props;
  const durationText = getDurationText(habit.duration);

  return (
    <ImageBackgroundCard
      imgSource={{ uri: getHabitImageUrl(habit.imageKey, 'w') }}
      accentColor={habit.color}
      title={habit.name}
      subtitle={durationText}
      rightIonIconName={'chevron-forward'}
    />
  );
});
