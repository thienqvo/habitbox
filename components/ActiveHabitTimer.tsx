import React from 'react';
import { observer } from 'mobx-react';
import { Animated } from 'react-native';
import { useTheme } from 'styled-components';
import Svg from 'react-native-svg';
import FastImage from 'react-native-fast-image';

import { usePrevious } from '../hooks/usePrevious';
import { View, TouchableOpacity } from './system/views';
import { Text } from './system/typography';
import { getDurationText } from './utils';
import { IonIcon } from './system/IonIcon';
import {
  AnimatedPath,
  describeArc,
  AnimatedPathRef,
} from './system/animated/AnimatedPath';
import { defaultTheme } from '../theme';
import { getHabitImageUrl } from '../utils/images';

interface ActiveHabitTimerProps {
  timerId: string;
  status: 'ACTIVE' | 'PAUSED';
  title: string;
  imageKey: string;
  accentColor: string;
  remainDuration: number;
  /**
   * Between 0-1
   */
  progressPercentage: number;
  /**
   * Calls when the timer it has exceeds X seconds
   */
  onDurationChange: (
    newDuration: number,
    currentDuration: number,
    timerId: string
  ) => void;
  onPause: (timerId: string) => void;
  onUnpause: (timerId: string) => void;
  onAddMoreTimePress: (
    secondsAdded: number,
    currentDuration: number,
    timerId: string
  ) => void;
  onCompletePress: (leftoverDuration: number, timerId: string) => void;
  options?: {};
}

export const ActiveHabitTimer: React.FC<ActiveHabitTimerProps> = observer(
  (props) => {
    const {
      timerId,
      status,
      title,
      imageKey,
      accentColor,
      remainDuration,
      progressPercentage,
      onDurationChange,
      onPause,
      onUnpause,
      onAddMoreTimePress,
      onCompletePress,
    } = props;

    const theme = useTheme();

    const animatedProgressPercent = React.useRef(
      new Animated.Value(progressPercentage)
    ).current;
    const pathRef = React.useRef<AnimatedPathRef>(null);
    const arc = React.useRef(new Animated.Value(0)).current;
    const previousTimerId = usePrevious(timerId);

    // derived
    const isTimerIdChanged = previousTimerId && previousTimerId !== timerId;

    // Animation springs
    const arcToFullSpring = React.useRef(
      Animated.spring(arc, {
        toValue: 358,
        stiffness: 80,
        damping: 20,
        mass: 3,
        overshootClamping: true,
        useNativeDriver: true,
      })
    ).current;
    const arcToFullFinishedRef = React.useRef(false);
    const arcToEmptySpring = React.useRef(
      Animated.spring(arc, {
        toValue: 0,
        stiffness: 100,
        damping: 20,
        mass: 3,
        overshootClamping: true,
        useNativeDriver: true,
      })
    ).current;

    const startAnimation = React.useCallback(
      (cb?: (finished: boolean) => void) => {
        arcToEmptySpring.reset();
        arcToFullSpring.start(({ finished }) => {
          arcToFullFinishedRef.current = finished;
          cb?.(finished);
        });
      },
      []
    );
    const rollbackAnimation = React.useCallback(
      (cb?: (finished: boolean) => void) => {
        if (arcToFullFinishedRef.current) {
          arcToFullSpring.reset();
          arcToFullFinishedRef.current = false;
        }
        arcToEmptySpring.start(({ finished }) => {
          cb?.(finished);
        });
      },
      []
    );

    // effects
    /**
     * Effect to update timer
     */
    React.useEffect(() => {
      let timeout: NodeJS.Timeout | null = null;
      if (status === 'ACTIVE') {
        timeout = setTimeout(() => {
          onDurationChange(remainDuration - 1, remainDuration, timerId);
        }, STEP_DURATION_MS);
      }
      return () => {
        if (timeout) {
          clearTimeout(timeout);
        }
      };
    }, [timerId, remainDuration, status]);

    React.useEffect(() => {
      if (!isTimerIdChanged) {
        Animated.timing(animatedProgressPercent, {
          duration: PROGRESS_BAR_ANIMATION_DURATION,
          useNativeDriver: false,
          toValue: progressPercentage,
        }).start();
      } else {
        animatedProgressPercent.setValue(progressPercentage);
      }
    }, [timerId, progressPercentage]);
    const defaultArc = describeArc(38, 38, PRIMARY_ICON_SIZE / 2, 0, 0);
    React.useEffect(() => {
      arc.addListener(({ value }) => {
        pathRef.current?.setNativeProps({
          d: describeArc(38, 38, PRIMARY_ICON_SIZE / 2, 0, value),
          opacity: value === 0 ? 0 : 1,
        });
      });
      return () => {
        arcToFullSpring.reset();
        arcToEmptySpring.reset();
        arcToFullFinishedRef.current = false;
        arc.setValue(0);
        arc.removeAllListeners();
      };
    }, [timerId]);

    return (
      <View testID={'container'} alignItems={'center'}>
        <View
          testID={'active-habit-container'}
          borderBottomRightRadius={32}
          borderBottomLeftRadius={32}
          backgroundColor={'night'}
          width={'100%'}
          alignItems={'center'}
        >
          <TouchableOpacity
            onPress={() =>
              status === 'PAUSED' ? onUnpause(timerId) : onPause(timerId)
            }
          >
            <View
              position={'relative'}
              width={208}
              height={208}
              mt={8}
              mx={52}
              borderRadius={16}
              overflow={'hidden'}
            >
              <FastImage
                source={{ uri: getHabitImageUrl(imageKey, 's') }}
                resizeMode={'cover'}
                style={{
                  width: 208,
                  height: 208,
                }}
              />
              {status === 'PAUSED' && (
                <View
                  position={'absolute'}
                  top={0}
                  right={0}
                  left={0}
                  bottom={0}
                  backgroundColor={'night-75'}
                  justifyContent={'center'}
                  alignItems={'center'}
                >
                  <IonIcon name={'pause'} color={'cumulus'} size={120} />
                </View>
              )}
            </View>
          </TouchableOpacity>
          <Text mt={3} size={'xl'} color={'cumulus'} weight={'medium'}>
            {title}
          </Text>
          <Text size={'timerSize'} color={'cumulus'} mt={4}>
            {getDurationText(remainDuration)}
          </Text>
          <View
            testID={'progress-bar'}
            backgroundColor={'shilling'}
            width={PROGRESS_BAR_WIDTH}
            height={PROGRESS_BAR_HEIGHT}
            borderRadius={25}
            overflow={'hidden'}
            mt={2}
          >
            <Animated.View
              style={{
                width: PROGRESS_BAR_WIDTH,
                height: PROGRESS_BAR_HEIGHT,
                borderRadius: 25,
                backgroundColor: accentColor,
                transform: [
                  {
                    translateX: animatedProgressPercent.interpolate({
                      inputRange: [0, 1],
                      outputRange: [-PROGRESS_BAR_WIDTH, 1],
                      extrapolate: 'clamp',
                    }),
                  },
                ],
              }}
            />
          </View>
          <View
            testID={'control'}
            flexDirection={'row'}
            alignItems={'flex-end'}
            mt={6}
            width={'100%'}
            position={'relative'}
          >
            <TouchableOpacity
              onPress={() =>
                status === 'PAUSED' ? onUnpause(timerId) : onPause(timerId)
              }
              style={{
                marginBottom: 16,
                flex: 1,
              }}
            >
              <Text color={'shilling'} textAlign={'center'}>
                {status === 'PAUSED' ? 'Play' : 'Pause'}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                onAddMoreTimePress(5 * 60, remainDuration, timerId)
              }
              style={{
                flex: 1,
                marginBottom: 16,
              }}
            >
              <Text
                testID={'add-more-time'}
                color={'shilling'}
                textAlign={'center'}
              >
                {'+ 5m'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View height={PRIMARY_ICON_SIZE - 4 - 40}>
          <Svg
            height={PRIMARY_ICON_SIZE + 4}
            width={PRIMARY_ICON_SIZE + 4}
            style={{
              position: 'absolute',
              top: -2,
              left: -2,
              transform: [
                {
                  translateY: -40,
                },
              ],
            }}
          >
            <AnimatedPath
              d={defaultArc}
              opacity={0}
              strokeWidth={2}
              stroke={defaultTheme.colors.white}
              strokeLinecap={'round'}
              ref={pathRef}
            />
          </Svg>
          <TouchableOpacity
            // onPress={() => onCompletePress(remainDuration, timerId)}
            onLongPress={() =>
              startAnimation((finished) => {
                if (finished) {
                  onCompletePress(remainDuration, timerId);
                }
              })
            }
            onPressOut={() => {
              rollbackAnimation();
            }}
            style={{
              transform: [
                {
                  translateY: -40,
                },
              ],
            }}
            activeOpacity={0.75}
          >
            <View
              testID={'checkmark'}
              borderRadius={PRIMARY_ICON_SIZE / 2}
              height={PRIMARY_ICON_SIZE}
              width={PRIMARY_ICON_SIZE}
              backgroundColor={accentColor}
              borderWidth={4}
              borderColor={'beluga'}
            >
              <IonIcon
                name={'checkmark'}
                size={PRIMARY_ICON_SIZE - 8}
                color={theme.colors.beluga}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
);

const STEP_DURATION_MS = 1000;
const PROGRESS_BAR_ANIMATION_DURATION = 200;
const PROGRESS_BAR_WIDTH = 328;
const PROGRESS_BAR_HEIGHT = 6;
const PRIMARY_ICON_SIZE = 72;
