
import React from 'react';
import { observer } from 'mobx-react';
import { SvgProps } from 'react-native-svg';

import CatDollKitty from '../icons/cat-doll-kitty.svg';

interface HbIconProps extends SvgProps {
  name: HbIconsNames;
}

const HbIconLookupTable: Record<HbIconsNames, React.FC<SvgProps>> = {
  'cat-doll-kitty': CatDollKitty
};

export const HbIcon: React.FC<HbIconProps> = observer((props) => {
  const { name, ...otherProps } = props;
  const Component = HbIconLookupTable[name];
  return <Component {...otherProps} />;
});

export const HbIconNamesList = [
  'cat-doll-kitty'
];
export type HbIconsNames = 'cat-doll-kitty';


export default HbIcon;
export { default as CatDollKitty } from '../icons/cat-doll-kitty.svg';
