import React from 'react';
import { observer } from 'mobx-react';

import { TouchableOpacity } from 'react-native';
import { View } from './system/views';
import { IonIcon } from './system/IonIcon';

interface HeaderProps {
  goBack: () => void;
  backgroundColor?: string;
  RightItem?: React.ComponentType<any>;
}

export const Header: React.FC<HeaderProps> = observer(
  ({ goBack, backgroundColor = '#4E4E53', RightItem }) => (
    <View
      testID={'header-container'}
      width={'100%'}
      backgroundColor={backgroundColor}
      height={50}
      justifyContent={'space-between'}
      alignItems={'center'}
      flexDirection={'row'}
      px={3}
    >
      <TouchableOpacity onPress={goBack}>
        <IonIcon
          name={'chevron-back'}
          color={'cumulus'}
          size={40}
          style={{ flexShrink: 0 }}
        />
      </TouchableOpacity>
      {RightItem && <RightItem />}
    </View>
  )
);
