import React from 'react';
import { observer } from 'mobx-react';
import { chunk } from 'lodash';
import { TouchableOpacity } from 'react-native';

import { HbIconsNames, HbIconNamesList } from './HbIcons';
import { ScrollView, View } from './system/views';
import HbIcon from './HbIcons';

const getIconPages = (iconPerRow: number, rowPerPage: number) =>
  chunk(chunk(HbIconNamesList, iconPerRow), rowPerPage);

export interface HbIconListProps {
  iconPerRow: number;
  rowPerPage: number;
  iconSize: number;
  selectedIconName: string;
  onPressIcon: (name: HbIconsNames) => void;
}

export const HbIconList: React.FC<HbIconListProps> = observer((props) => {
  const {
    iconPerRow,
    rowPerPage,
    iconSize,
    onPressIcon,
    selectedIconName,
  } = props;

  const iconPages = React.useMemo(() => getIconPages(iconPerRow, rowPerPage), [
    iconPerRow,
    rowPerPage,
  ]);

  return (
    <ScrollView
      flexDirection={'row'}
      flexWrap={'nowrap'}
      horizontal
      showsHorizontalScrollIndicator={false}
    >
      {iconPages.map((rows, index) => (
        <View key={'icon_page' + index}>
          {rows.map((row, index) => (
            <View
              key={'icon_row' + index}
              flexDirection={'row'}
              flexWrap={'nowrap'}
              mt={index === 0 ? 0 : 2}
              alignItems={'baseline'}
              minHeight={iconSize}
            >
              {row.map((name, index) => (
                <TouchableOpacity
                  key={name}
                  onPress={() => onPressIcon(name as HbIconsNames)}
                >
                  <HbIcon
                    key={name}
                    name={name as HbIconsNames}
                    width={iconSize}
                    height={iconSize}
                    style={{
                      flexShrink: 0,
                      marginLeft: index === 0 ? 0 : 8,
                      opacity: selectedIconName === name ? 1 : 0.5,
                    }}
                  />
                </TouchableOpacity>
              ))}
            </View>
          ))}
        </View>
      ))}
    </ScrollView>
  );
});
