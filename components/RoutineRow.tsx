import React from 'react';
import { TouchableHighlight, ViewProps, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react';
import { useTheme } from 'styled-components';

import { View } from './system/views';
import { Text } from './system/typography';
import { IRoutine } from '../stores/UserStore';
import { IllustrationVectors, IllustrationNames } from './IllustrationVectors';

interface RoutineRowProps extends Pick<ViewProps, 'style'> {
  routine: IRoutine;
  onPress: (routine: IRoutine) => void;
  onStart: (routine: IRoutine) => void;
}
export const RoutineRow: React.FC<RoutineRowProps> = observer(
  ({ routine, onPress, onStart, style }) => {
    const theme = useTheme();

    const { title, displayInfo } = routine;

    return (
      <TouchableHighlight
        onPress={() => onPress?.(routine)}
        underlayColor={theme.colors.possum}
        style={style}
      >
        <View backgroundColor={'night'} px={4} py={6} borderRadius={8}>
          <View flexDirection={'row'}>
            <View>
              <IllustrationVectors
                name={displayInfo.illustrationName as IllustrationNames}
                primary={displayInfo.primaryColor}
                secondary={displayInfo.secondaryColor}
                width={100}
                height={100}
              />
            </View>
            <View
              ml={3}
              justifyContent={'center'}
              flex={1}
              alignItems={'flex-start'}
            >
              <Text
                size={'lg'}
                weight={'bold'}
                color={'cumulus'}
                numberOfLines={1}
                ellipsizeMode={'tail'}
              >
                {title}
              </Text>
              <Text size={'md'} weight={'regular'} color={'cumulus'} mt={2}>
                {routine.totalDurationString}
              </Text>
              <TouchableOpacity onPress={() => onStart(routine)}>
                <View
                  mt={2}
                  px={4}
                  py={1}
                  backgroundColor={
                    routine.habits.length > 0
                      ? displayInfo.primaryColor
                      : theme.colors.possum
                  }
                  opacity={routine.habits.length > 0 ? 1 : 0.5}
                  borderRadius={8}
                >
                  <Text size={'md'} weight={'bold'} color={'white'}>
                    {'Start'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
);
