export const getDurationText = (duration: number) => {
  if (duration < 60) {
    return `${duration}s`;
  }
  if (duration < 3600) {
    const seconds = duration % 60;
    return `${Math.floor(duration / 60)}m ${seconds}s`;
  }
  const minutes = Math.floor((duration % 3600) / 60);
  return `${Math.floor(duration / 3600)}h ${minutes}m`;
};
