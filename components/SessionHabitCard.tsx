import React from 'react';
import { observer } from 'mobx-react';

import { ISessionHabit } from '../stores/UserStore';
import { ImageBackground, View } from './system/views';
import { Text } from './system/typography';
import { getDurationText } from './utils';
import { ViewProps } from 'react-native';
import { getHabitImageUrl } from '../utils/images';

interface SessionHabitCardProps {
  sessionHabit: ISessionHabit;
  style?: ViewProps['style'];
}

export const SessionHabitCard: React.FC<SessionHabitCardProps> = observer(
  (props) => {
    const { sessionHabit, style } = props;
    return (
      <ImageBackground
        source={{ uri: getHabitImageUrl(sessionHabit.displayImageKey, 's') }}
        p={3}
        borderRadius={6}
        overflow={'hidden'}
        resizeMode={'cover'}
        opacity={sessionHabit.hasStatusOf('COMPLETED', 'SKIPPED') ? 0.5 : 1}
        style={style}
      >
        <View
          position={'absolute'}
          backgroundColor={'night-80'}
          top={0}
          right={0}
          left={0}
          bottom={0}
        />
        {sessionHabit.status === 'ACTIVE' && (
          <View
            position={'absolute'}
            mt={3}
            mr={3}
            top={0}
            right={0}
            px={3}
            py={1}
            backgroundColor={'black-75'}
            borderRadius={24}
          >
            <Text color={'cumulus'} weight={'semibold'} size={'xs'}>
              {'Active'}
            </Text>
          </View>
        )}
        <View>
          <View
            borderTopRightRadius={6}
            borderBottomRightRadius={6}
            backgroundColor={sessionHabit.accentColor}
            width={120}
            height={6}
          />

          <Text color={'cumulus'} mt={2} weight={'medium'}>
            {sessionHabit.displayName}
          </Text>
          <Text color={'cumulus'} mt={1}>
            {getDurationText(sessionHabit.originalDuration)}
          </Text>
        </View>
      </ImageBackground>
    );
  }
);
